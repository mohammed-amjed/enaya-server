

### Intro ###

Enaya's backend server base built with Express.js and PostgreSQL

### Installation ###

* Clone the repo
* cd to current project directory 
* run npm install
* run npm run start to start the local dev server at port 3000
* Access through localhost:3000 
