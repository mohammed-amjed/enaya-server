/*
 Navicat Premium Data Transfer

 Source Server         : locahost
 Source Server Type    : PostgreSQL
 Source Server Version : 140001
 Source Host           : localhost:5432
 Source Catalog        : enaya
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 140001
 File Encoding         : 65001

 Date: 05/12/2021 02:33:04
*/


-- ----------------------------
-- Sequence structure for users_id_seq
-- ----------------------------
DROP SEQUENCE IF EXISTS "public"."users_id_seq";
CREATE SEQUENCE "public"."users_id_seq" 
INCREMENT 1
MINVALUE  1
MAXVALUE 2147483647
START 1
CACHE 1;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "full_name" varchar(255) COLLATE "pg_catalog"."default",
  "phone" varchar(15) COLLATE "pg_catalog"."default",
  "otp" varchar(6) COLLATE "pg_catalog"."default",
  "user_type" int4,
  "created_at" timestamp(6),
  "last_login" timestamp(6),
  "imei" varchar(15) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "public"."users" VALUES (1, 'Mohamed Amjed elmardi', '0919864004', 'K6Oj4a', 2, '2020-01-21 02:52:52', '2005-07-02 20:48:09', '568475982135425');
INSERT INTO "public"."users" VALUES (2, 'Hiba Sharif', '0912342342', 'Fsc3', 1, '2021-10-15 14:31:21', '2021-12-05 02:31:33', '845557845548265');

-- ----------------------------
-- Alter sequences owned by
-- ----------------------------
ALTER SEQUENCE "public"."users_id_seq"
OWNED BY "public"."users"."id";
SELECT setval('"public"."users_id_seq"', 5, true);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "users_pkey" PRIMARY KEY ("id");
