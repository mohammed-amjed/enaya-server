const express = require("express");
const cors = require("cors");
const usersRoutes = require("./src/users/routes");

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());


app.get("/", (req, res) => {
  res.send("up and running");
});

//api route for users
app.use("/api/v1/", usersRoutes);

app.listen(port, () => console.log(`app listening on port ${port}`));
