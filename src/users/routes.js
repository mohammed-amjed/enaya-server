const { Router } = require("express");
const controller = require("./controller");

const router = Router();

//users routes
router.get("/", controller.getUsers);
router.post("/", controller.addUsers);
router.get("/:id", controller.getUserById);
router.put("/:id", controller.updateUser);
router.delete("/:id", controller.deleteUser);

module.exports = router;
