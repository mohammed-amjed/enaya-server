// All database quies goes here!
const getUsers = "SELECT * FROM users";
const getUserById = "SELECT * FROM users WHERE id= $1";
const checkPhoneExists = "SELECT u FROM users u WHERE u.phone = $1";
const addUser =
  "INSERT INTO users (full_name, phone, otp, user_type, created_at, last_login, imei) VALUES ($1, $2, $3, $4, $5, $6, $7)";

const updateUser = "UPDATE users set full_name = $1 WHERE id = $2";
const deleteUser = "DELETE FROM users WHERE id = $1";
module.exports = {
  getUsers,
  getUserById,
  checkPhoneExists,
  addUser,
  updateUser,
  deleteUser,
};
