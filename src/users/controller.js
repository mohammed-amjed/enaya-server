const pool = require("../../db");
const queries = require("./queries");

/**
 * Get all users function from db
 */

const getUsers = (req, res) => {
  pool.query(queries.getUsers, (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

/**
 * Get user by id function
 *
 */
const getUserById = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getUserById, [id], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

/**
 * Add user function
 *
 */
const addUsers = (req, res) => {
  const { full_name, phone, otp, user_type, created_at, last_login, imei } =
    req.body;
  //check if phone number already exits
  pool.query(queries.checkPhoneExists, [phone], (error, results) => {
    if (results.rows.length) {
      res.send("phone already registered.");
    }
    pool.query(
      queries.addUser,
      [full_name, phone, otp, user_type, created_at, last_login, imei],
      (error, results) => {
        if (error) throw error;
        res.status(201).send("User added successfully!");
      }
    );
  });
};

/**
 * update user function
 * update full name only for now
 * based on id
 */
const updateUser = (req, res) => {
  const id = parseInt(req.params.id);
  const { full_name } = req.body;

  pool.query(queries.getUserById, [id], (error, results) => {
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("User does not exit,could not update");
    }
  });

  pool.query(queries.updateUser, [full_name, id], (error, results) => {
    if (error) throw error;
    res.status(200).send("User updated successfully");
  });
};

/**
 * delete user function
 * based on id
 */
const deleteUser = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getUserById, [id], (error, results) => {
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("User does not exit,could not delete");
    }
  });

  pool.query(queries.deleteUser, [id], (error, results) => {
    if (error) throw error;
    res.status(200).send("User removed successfully!");
  });
};

module.exports = {
  getUsers,
  getUserById,
  addUsers,
  updateUser,
  deleteUser,
};
