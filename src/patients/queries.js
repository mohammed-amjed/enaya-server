// All database quies goes here!
const getPatients = "SELECT * FROM Patient";
const getPatientById = "SELECT * FROM Patient WHERE id= $1";
const checkPatientExists = "SELECT u FROM Patient u WHERE u.id = $1";
const addPatient =
  "INSERT INTO users (uid, nextSupervisor_id, stateoflife) VALUES ($1, $2, $3)";

const updatePatient = "UPDATE Patient set stateoflife = $1 WHERE id = $2";
const deletePatient = "DELETE FROM Patient WHERE id = $1";
module.exports = {
  getPatients,
  getPatientById,
  checkPatientExists,
  addPatient,
  updatePatient,
  deletePatient,
};
