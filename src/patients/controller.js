const pool = require("../../db");
const queries = require("./queries");

/**
 * Get all patient function from db
 */

const getPatients = (req, res) => {
  pool.query(queries.getPatients, (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

/**
 * Get patient by id function
 *
 */
const getPatientById = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getPatientById, [id], (error, results) => {
    if (error) throw error;
    res.status(200).json(results.rows);
  });
};

/**
 * Add patient function
 *
 */
const addPatients = (req, res) => {
  const { uid, nextSupervisor_id, stateoflife } = req.body;
  //check if phone number already exits
  pool.query(queries.checkPatientExists, [uid], (error, results) => {
    if (results.rows.length) {
      res.send("patient already exists.");
    }
    pool.query(
      queries.addPatient,
      [uid, nextSupervisor_id, stateoflife],
      (error, results) => {
        if (error) throw error;
        res.status(201).send("Patient added successfully!");
      }
    );
  });
};

/**
 * update patient function
 * update state of life
 * based on id & uid
 */
const updatePatient = (req, res) => {
  const id = parseInt(req.params.id);
  const { stateoflife } = req.body;

  pool.query(queries.getPatientById, [id], (error, results) => {
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("Patient does not exit,could not update");
    }
  });

  pool.query(queries.updatePatient, [stateoflife, id], (error, results) => {
    if (error) throw error;
    res.status(200).send("Patient updated successfully");
  });
};

/**
 * delete user function
 * based on id
 */
const deletePatient = (req, res) => {
  const id = parseInt(req.params.id);
  pool.query(queries.getPatientById, [id], (error, results) => {
    const noUserFound = !results.rows.length;
    if (noUserFound) {
      res.send("Patient does not exit,could not delete");
    }
  });

  pool.query(queries.deletePatient, [id], (error, results) => {
    if (error) throw error;
    res.status(200).send("Patient removed successfully!");
  });
};

module.exports = {
  getPatients,
  getPatientById,
  addPatients,
  updatePatient,
  deletePatient,
};
