const { Router } = require("express");
const controller = require("./controller");

const router = Router();

//patients routes
router.get("/patients", controller.getPatients);
router.post("/patients", controller.addPatients);
router.get("/patients/:id", controller.getPatientById);
router.put("/patient/:id", controller.updatePatient);
router.delete("/patient:id", controller.deletePatient);

module.exports = router;
